package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

const correctPassword string = "123"

type Credential struct {
	Password string `json:"password"`
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Unsupported Method! Use POST!\n"))
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		w.Write([]byte("Unsupported Media Type! Use application/json!\n"))
		return
	}

	data, err := io.ReadAll(r.Body)

	if err != nil {
		log.Println(err.Error())
		return
	}

	if len(data) == 0 {
		w.WriteHeader(http.StatusLengthRequired)
		w.Write([]byte("You cannot send empty request!\n"))
		return
	}

	credential := &Credential{}
	err = json.Unmarshal(data, credential)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Incorrect json data!\n"))
		return
	}

	if credential.Password == correctPassword {
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte("Success! You are authorized!\n"))
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Fail! You are not authorized!\n"))
	}
}

func main() {

	port := ":8080"

	fmt.Printf("HTTP Server started at port %s\n", port)

	http.HandleFunc("/", homeHandler)

	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatalln(err.Error())
	}
}
